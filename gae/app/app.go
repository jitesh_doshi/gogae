package app

import (
	"appengine"
	"appengine/datastore"
	"appengine/mail"
	"appengine/xmpp"
	"fmt"
	"net/http"
	"text/template"
	"time"
)

func init() {
	http.HandleFunc("/guestbook", guestbook)
}

const pageHTML = `
{{$r := index . 0}}
{{$d := index . 1}}
<html>
 <head>
  <link rel="stylesheet" href="_f/css/style.css" type="text/css"></link>
 </head>
 <body>
  <form method="post">
    Your Name: <input type="text" name="name" value="{{$r.FormValue "name"}}"><br>
    Your Comment: <textarea name="comment"></textarea><br>
    <input type="submit" value="submit">
  </form>
  {{with $r.FormValue "name"}}
  <p>Thanks for visiting - {{.}}.</p>
  {{end}}
  {{with $d}}
  <h3>Past Visitors</h3>
  <table>
   <thead>
    <th>Name</th>
    <th>Comment</th>
    <th>Date</th>
   </thead>
   <tbody>
   {{range .}}
    <tr>
	 <td>{{html .Name}}</td>
	 <td>{{html .Comment}}</td>
	 <td>{{html .Date}}</td>
	</tr>
   {{end}}
   </tbody>
  </table>
  {{end}}
 </body>
</html>
`

var pageTemplate = template.Must(template.New("guestbook").Parse(pageHTML))

type Visitor struct {
	Name    string
	Comment string
	Date    time.Time
}

func guestbook(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	c.Infof(`Got a request with name "%s" and comment "%s"`, r.FormValue("name"), r.FormValue("comment"))
	if r.FormValue("name") != "" {
		v := Visitor{
			r.FormValue("name"),
			r.FormValue("comment"),
			time.Now(),
		}

		key := datastore.NewIncompleteKey(c, "Guestbook", nil)
		if _, err := datastore.Put(c, key, &v); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		send_mail(c, v.Name)
		send_xmpp(c, v.Name)
	}
	q := datastore.NewQuery("Guestbook").Order("-Date").Limit(10)
	visitors := make([]Visitor, 0, 10)
	if _, err := q.GetAll(c, &visitors); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err := pageTemplate.Execute(w, []interface{}{r, visitors})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func send_mail(c appengine.Context, name string) error {
	m := &mail.Message{
		Sender: "jitesh@doshiland.com",
		To: []string{"test@doshiland.com"},
		Subject: "guestbook signed",
		Body: fmt.Sprintf("%s just left you a guestbook message", name),
	}
	err := mail.Send(c, m)
	if err != nil {
		c.Errorf("Failed to send mail")
	}
	return err
}

func send_xmpp(c appengine.Context, name string) error {
	m := &xmpp.Message{
		// Sender: "jitesh@doshiland.com",
		To: []string{"jitesh@doshiland.com"},
		Body: fmt.Sprintf("%s just left you a guestbook message", name),
	}
	err := m.Send(c)
	if err != nil {
		c.Errorf("Failed to send IM")
	}
	return err
}
